#include <iostream>
#include "list.h" //increase list header
using namespace std;

int main()
{
	List list; //Declare the list
	int check, select, el, del,count_el=0;
	
	//Show this message
	cout << "==========================================================" << endl;
	cout << "[README] INPUT AT LEAST 4 ELEMENTS AND NO SAME ELEMENT!!! " << endl;
	cout << "==========================================================" << endl;

	//Loop for input each elements
	while (true)
	{
		//Show this message
		cout << "Please, input what type of push element. At least 4 elements and NO SAME element!!! " << endl;
		cout << "[1] Push head [2] Push tail [0] Stop pushing(Can work if number of elements >= 4) " << endl;
		cout << "Enter your select : ";
		cin >> select; //store what type of push element

		while (select != 1 && select != 2 && select != 0) //check if select from user incorrect and try again
		{
			cout << "Invaid input , Try again!" << endl;
			cout << "Enter your select : ";
			cin >> select;
		}
		cout << endl;
		
		//store the element
		if (select != 0)
		{
			cout << "Enter your element : "; 
			cin >> el;
			if (el > 0) 
				count_el++; //element count
			while (el == 0) //check if element is not correct and try again
			{
				cout << "Element mustn't equal to 0 ,Try again : ";
				cin >> el;
				if (el > 0)
					count_el++;
			}
		}
		cout << endl;

		if (select == 1) //headpush
			list.headPush(el);
		if (select == 2)//tailpush
			list.tailPush(el);
		if (select == 0 && count_el>=4) //stop loop
			break;
	}

	//show list
	if (!list.isEmpty()) {
		cout << "Your elements : "; list.showEl();
	cout << endl;
	
		cout << "Your headPop is : " << list.headPop() << endl; //show headpop
		cout << "Your tailPop is : " << list.tailPop() << endl; //show tailpop
		cout << endl;
		cout << "Input the element that you not want from list : ";
		cin >> del;
		while (del == 0)
		{
			cout << "The element to delete not equal to 0 ,Try again! : ";
			cin >> del;
		}
		if (del != 0)
			list.deleteNode(del); //delete element
		
		cout << "Input the element that you want to check in the list : ";
		cin >> check;
		while (check == 0)
		{
			cout << "The element to check not equal to 0 ,Try again! : ";
			cin >> check;
		}
		cout << endl;

		//check condition
		if (check != 0) {
			if (list.isInList(check) == true)
				cout << "The element is in the list." << endl;
			else
				cout << "The element isn't in the list." << endl;
		}
	}
system("pause");
return 0;
}