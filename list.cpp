#include <iostream>
#include "list.h"
using namespace std;
//deconstructor
List::~List() {
	for(Node *p; !isEmpty(); ){
		p=head->next;
		delete head;
		head=p;
	}
}

//headpush
void List::headPush(int el)
{
	Node *tmp = new Node(el); //declare new node
	if (head == NULL) 
	{
		head = tmp;
		tail = head;
	}
	else
	{
		tmp->next = head;
		head = tmp;
	}
}

//tailpush
void List::tailPush(int el)
{
	Node *tmp = new Node(el);
	if (tail == NULL)
	{
		tail = tmp;
		head = tail;
	}
	else
	{
		tail->next = tmp;
		tail = tmp;
	}
}

int List::tailPop()
{
	Node *now = new Node(NULL); //declare new node
	Node *prev = new Node(NULL);
	now = head; //make node name now point to head
	while (now->next != NULL)
	{
		prev = now; //making prev point previous element
		now = now->next; //change point
	}
	tail = prev; //make tail point to prev
	int value = now->info; //store value that now pointing
	prev->next=head; //next prev point to head
	delete now; //delete element that now pointing
	return value; // return value that now pointing before deleted
	
}

//headpop
int List::headPop()
{
	Node *tmp = new Node(*head);
	int holdvalue = head->info; //store value that head pointing
	tmp = head; //making node name tmp point to head
	head = head->next; //head go next position
	delete tmp; //delete element that tmp pointing
	return holdvalue;
}

void List::deleteNode(int el)
{
	Node *tmp = head; //make new node point to head
	Node *tmp2 = head;

	if (head == NULL) //if no element show this
	{
		cout << "No element remaining!!" << endl;
	}
	else
	{
		if (head == tail) //if element remain 1 do this
		{
			if (tmp->info == el)
			{
				head = NULL; //make head and tail point to NULL
				tail = NULL;//
				delete tmp; //delete element
			}
		}
		else
		{
			if (tail->info == el) //if element at tail equal to element that users need to delete
			{
				while (tmp2->next != tail)//find element previous tail
				{
					tmp2 = tmp2->next;
				}
				tail = tmp2; 
				tail->next = NULL;
				tmp2 = tmp2->next;
				delete tmp2;
			}
			else
			{
				if (head->info == el)  //if element at head equal to element that users need to delete
				{
					head = head->next; //make head point next position
					delete tmp;
				}
				else
				{
					if (tmp->next->next != NULL) { //to delete element between of head and tail
						while (tmp->next->next != NULL)
						{
							if (tmp->next->info == el)
							{
								tmp2 = tmp->next;
								tmp->next = tmp2->next;
								delete tmp2;
							}
							else
								tmp = tmp->next; 
						}
					}
				}
			}
		}
	}
}

bool List::isInList(int el)
{
	Node *tmp = head;
	//codition that check element is in list True or false
	if (head == NULL || tmp==NULL)
	{
		return false;
	}
	else 
	{
		while (tmp != NULL)
		{
			if (tmp->info == el)
				return true;
			else
				tmp = tmp->next; //to find anoter elements

		}
		return false;
	}
}

void List::showEl() //show elements
{
	Node *tmp = head;
	if (head == NULL)
		cout << "Empty!!." << endl;
	else {
		while (tmp != NULL)
		{
			cout << tmp->info << " ";
			tmp = tmp->next;
		}
	}
}